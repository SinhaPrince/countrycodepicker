//
//  Languages.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 14, 2021
//
import Foundation
import SwiftyJSON

struct Languages {

	let iso6391: String?
	let iso6392: String?
	let name: String?
	let nativeName: String?

	init(_ json: JSON) {
		iso6391 = json["iso639_1"].stringValue
		iso6392 = json["iso639_2"].stringValue
		name = json["name"].stringValue
		nativeName = json["nativeName"].stringValue
	}

}