//
//  CountryCodeDataModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 14, 2021
//
import Foundation
import SwiftyJSON

struct CountryCodeDataModel {

	let name: String?
	let topLevelDomain: [String]?
	let alpha2Code: String?
	let alpha3Code: String?
	let callingCodes: [String]?
	let capital: String?
	let altSpellings: [String]?
	let region: String?
	let subregion: String?
	let population: Int?
	let latlng: [Int]?
	let demonym: String?
	let area: Int?
	let gini: Double?
	let timezones: [String]?
	let borders: [String]?
	let nativeName: String?
	let numericCode: String?
	let currencies: [Currencies]?
	let languages: [Languages]?
	let translations: Translations?
	let flag: String?
    var flagIcon: UIImage?
	let regionalBlocs: [RegionalBlocs]?
	let cioc: String?

	init(_ json: JSON) {
		name = json["name"].stringValue
		topLevelDomain = json["topLevelDomain"].arrayValue.map { $0.stringValue }
		alpha2Code = json["alpha2Code"].stringValue
		alpha3Code = json["alpha3Code"].stringValue
		callingCodes = json["callingCodes"].arrayValue.map { $0.stringValue }
		capital = json["capital"].stringValue
		altSpellings = json["altSpellings"].arrayValue.map { $0.stringValue }
		region = json["region"].stringValue
		subregion = json["subregion"].stringValue
		population = json["population"].intValue
		latlng = json["latlng"].arrayValue.map { $0.intValue }
		demonym = json["demonym"].stringValue
		area = json["area"].intValue
		gini = json["gini"].doubleValue
		timezones = json["timezones"].arrayValue.map { $0.stringValue }
		borders = json["borders"].arrayValue.map { $0.stringValue }
		nativeName = json["nativeName"].stringValue
		numericCode = json["numericCode"].stringValue
		currencies = json["currencies"].arrayValue.map { Currencies($0) }
		languages = json["languages"].arrayValue.map { Languages($0) }
		translations = Translations(json["translations"])
		flag = json["flag"].stringValue
		regionalBlocs = json["regionalBlocs"].arrayValue.map { RegionalBlocs($0) }
		cioc = json["cioc"].stringValue
        flagIcon = UIImage(systemName: "flag")!
	}

}
