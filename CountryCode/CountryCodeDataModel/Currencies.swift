//
//  Currencies.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 14, 2021
//
import Foundation
import SwiftyJSON

struct Currencies {

	let code: String?
	let name: String?
	let symbol: String?

	init(_ json: JSON) {
		code = json["code"].stringValue
		name = json["name"].stringValue
		symbol = json["symbol"].stringValue
	}

}