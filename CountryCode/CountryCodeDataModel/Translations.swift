//
//  Translations.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 14, 2021
//
import Foundation
import SwiftyJSON

struct Translations {

	let de: String?
	let es: String?
	let fr: String?
	let ja: String?
	let it: String?
	let br: String?
	let pt: String?
	let nl: String?
	let hr: String?
	let fa: String?

	init(_ json: JSON) {
		de = json["de"].stringValue
		es = json["es"].stringValue
		fr = json["fr"].stringValue
		ja = json["ja"].stringValue
		it = json["it"].stringValue
		br = json["br"].stringValue
		pt = json["pt"].stringValue
		nl = json["nl"].stringValue
		hr = json["hr"].stringValue
		fa = json["fa"].stringValue
	}

}