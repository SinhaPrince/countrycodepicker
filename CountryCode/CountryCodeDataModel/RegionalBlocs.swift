//
//  RegionalBlocs.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 14, 2021
//
import Foundation
import SwiftyJSON

struct RegionalBlocs {

	let acronym: String?
	let name: String?
	let otherAcronyms: [Any]?
	let otherNames: [Any]?

	init(_ json: JSON) {
		acronym = json["acronym"].stringValue
		name = json["name"].stringValue
		otherAcronyms = json["otherAcronyms"].arrayValue.map { $0 }
		otherNames = json["otherNames"].arrayValue.map { $0 }
	}

}