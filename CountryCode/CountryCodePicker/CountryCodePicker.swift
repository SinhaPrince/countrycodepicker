//
//  CountryCodePicker.swift
//  CountryCode
//  Prince Sinha (iOS Developer)
//  Created by Cst on 3/15/21.
//

import UIKit
import SwiftyJSON

//MARK:- Main File(Class) to get all Country Info
//MARK:-

class CountryCodePicker: UIViewController,UISearchBarDelegate{
    
    
    var countryInfoArray = [CountryCodeDataModel]()
    var filterArray = [CountryCodeDataModel]()
    var delegate: GetCodeDelegate?
    
    lazy var tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = .clear
        return table
    }()
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setConstraint()
        apiService()
    }
    
    //MARK:- Set Constraints
    
    func setConstraint(){
        
        //Add tableView
        self.view.addSubview(self.tableView)
        self.tableView.frame = self.view.bounds
        
        //Setup Navigation
        let searchBar = UISearchBar()
        searchBar.sizeToFit()
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        self.navigationController?.navigationBar.topItem?.titleView = searchBar
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    /// Get All Country Info Using This Service
    func apiService(){
        let session = URLSession.shared
        let url = URL(string: "https://restcountries.eu/rest/v2/all")!
        
        let task = session.dataTask(with: url) { data, response, error in
            
            if error != nil || data == nil {
                print("Client error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                return
            }
            
            guard let mime = response.mimeType, mime == "application/json" else {
                print("Wrong MIME type!")
                return
            }
            
            let resJson = JSON(data ?? Data())
            if self.countryInfoArray.count > 0 {
                self.countryInfoArray.removeAll()
            }
            resJson.array?.forEach({ (item) in
                self.countryInfoArray.append(CountryCodeDataModel(item))
            })
            DispatchQueue.main.async {
                self.filterArray = self.countryInfoArray
                self.tableConfiguration()
            }
            
        }
        
        task.resume()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterArray = self.countryInfoArray.filter{ ($0.name?.contains(searchText))!}
        
        if searchText == ""{
            self.filterArray = self.countryInfoArray
        }
        self.tableConfiguration()
    }
}

//MARK:- UITableView Setup
//MARK:-
extension CountryCodePicker: UITableViewDelegate,UITableViewDataSource{
    
    func tableConfiguration(){
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.filterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.tableView.register(CountryTableCell.self, forCellReuseIdentifier: "CountryTableCell")
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "CountryTableCell") as? CountryTableCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        ( cell as? CountryTableCell)?.item = self.filterArray[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            self.delegate?.countryInfo(info: self.filterArray[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        45
    }
}

//MARK:- TableView Cell
//MARK:-
class CountryTableCell: UITableViewCell {
    
    
    lazy var flagIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var name: UILabel = {
        let lable = UILabel()
        lable.font = .systemFont(ofSize: 16, weight: .medium)
        lable.textColor = .black
        return lable
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setConstraint()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- Set Constaint
    
    func setConstraint(){
        
        contentView.addSubview(flagIcon)
        contentView.addSubview(name)
        flagIcon.translatesAutoresizingMaskIntoConstraints = false
        name.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([flagIcon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
                                     flagIcon.heightAnchor.constraint(equalToConstant: 60),
                                     flagIcon.widthAnchor.constraint(equalToConstant: 70),
                                     flagIcon.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([name.leadingAnchor.constraint(equalTo: flagIcon.trailingAnchor, constant: 10),
                                     name.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    
    // Country Info Data
    var item: CountryCodeDataModel?{
        didSet{
            setConstraint()
            name.text = item?.name
            let imageUrl = "https://www.countryflags.io/\(item?.alpha2Code ?? "")/flat/64.png"
            
            self.flagIcon.setImage(withImageId: imageUrl, placeholderImage: (UIImage(systemName: "flag")?.withTintColor(.black))!)
            
        }
    }
}

//MARK:-
/// To Get country info using this delegate
protocol GetCodeDelegate {
    func countryInfo(info:CountryCodeDataModel)
}

//MARK:- Helping Method To Download Image
private var activityIndicatorAssociationKey: UInt8 = 0

let imageCache = NSCache<AnyObject, AnyObject>()

enum ImageSize {
    case original
    case thumbnail
}

extension UIImageView {
    
    
    func setImage(withImageId imageId: String, placeholderImage: UIImage, size: ImageSize = .original) {
        
        cacheImage(urlString: imageId, placeholder: placeholderImage)
        
    }
    
    var activityIndicator: UIActivityIndicatorView! {
        get {
            return objc_getAssociatedObject(self, &activityIndicatorAssociationKey) as? UIActivityIndicatorView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &activityIndicatorAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func showActivityIndicator() {
        
        if self.activityIndicator == nil {
            self.activityIndicator = UIActivityIndicatorView(style: .gray)
            
            self.activityIndicator.hidesWhenStopped = true
            self.activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
            self.activityIndicator.style = .gray
            self.activityIndicator.center = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height / 2)
            self.activityIndicator.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
            self.activityIndicator.isUserInteractionEnabled = false
            
            OperationQueue.main.addOperation({ () -> Void in
                self.addSubview(self.activityIndicator)
                self.activityIndicator.startAnimating()
            })
        }
    }
    
    func hideActivityIndicator() {
        OperationQueue.main.addOperation({ () -> Void in
            self.activityIndicator.stopAnimating()
        })
    }
    
    func cacheImage(urlString: String, placeholder: UIImage) {
        
        self.showActivityIndicator()
        
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            self.hideActivityIndicator()
            return
        }
        
        let urlwithPercent = urlString.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        
        guard let url = URL(string: urlwithPercent ?? "")else{
            self.image = placeholder
            self.hideActivityIndicator()
            return
        }
        var urlRequest = URLRequest(url: url)
        //common headers
        //        urlRequest.setValue(ContentType.ENUS.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptLangauge.rawValue)
        //        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        //        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        self.image = placeholder
        URLSession.shared.dataTask(with: urlRequest) {data, _, _ in
            if data != nil {
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: data!)
                    if imageToCache != nil {
                        imageCache.setObject(imageToCache!, forKey: urlString as AnyObject)
                        self.image = imageToCache
                    }
                    self.hideActivityIndicator()
                }
            }
        }.resume()
    }
}
