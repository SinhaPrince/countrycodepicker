//
//  ViewController.swift
//  CountryCode
//  Prince Sinha (iOS Developer)
//  Created by Cst on 3/13/21.
//

import UIKit


class FirstVC: UIViewController,GetCodeDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbl_Code: UILabel!
    @IBOutlet weak var btnGetCode: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnGetCode.addTarget(self, action: #selector(btnGetCodeTap), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    @objc func btnGetCodeTap(){
        
        let myVC = CountryCodePicker()
        myVC.delegate = self
        let navController = UINavigationController(rootViewController: myVC)
        self.navigationController?.present(navController, animated: true, completion: nil)
    }
    
    func countryInfo(info: CountryCodeDataModel) {
        let imageUrl = "https://www.countryflags.io/\(info.alpha2Code ?? "")/flat/64.png"
        imageView.setImage(withImageId: imageUrl , placeholderImage: UIImage(systemName: "flag")!)
        
        self.btnGetCode.setTitle(info.callingCodes?.first, for: .normal)
        self.btnGetCode.setImage(imageView.image, for: .normal)
    }
    
}


